# pvlc

## video 

```
ffmpeg -i test-ecran-1280x800-60fps.mp4 -vcodec libx264 -profile:v high -preset slow -crf 18 -b-pyramid none -acodec ac3 -ab 1536k -scodec copy test-ecran-1280x800-60fps-ffmpeg.mp4 
```

## from lite

### raspi-config
console autologin

```
sudo apt install  python3-pip micro git tmux vlc 
```

## python osc player 

### 
```
pip install python-vlc python-osc
```
#### avahi-ssh
```
sudo su
curl https://raw.githubusercontent.com/lathiat/avahi/master/avahi-daemon/ssh.service > /etc/avahi/services/ssh.service
exit
sudo systemctl restart avahi-daemon.service 
```

## samba 
### install
```
sudo apt-get update  && \
sudo apt-get install samba samba-common-bin && \
sudo smbpasswd -a pi 
sudo service smbd restart
```


### rw home folder

```
micro /etc/samba/smb.conf 
```
~line 176
```diff
- read only = yes
+ read only = no
```

```bash
sudo systemctl restart smbd.service 
```

## config TXT

comment out at line ~60 
```
dtoverlay=vc4-fkms-v3d
```
enable the fake kms

dtoverlay=vc4-fkms-v3d

dtoverlay=vc4-fkms-v3d,cma-256



### USB C serial

```
dtoverlay=dwc2,dr_mode=host
dtoverlay=vc4-fkms-v3d,cma-256


### Fix poe fan curve 
# PoE Hat Fan Speeds
dtparam=poe_fan_temp0=50000
dtparam=poe_fan_temp1=60000
dtparam=poe_fan_temp2=70000
dtparam=poe_fan_temp3=80000
```

## midimonster
?



add user to audio and dialout group
```
sudo usermod -aG audio,video,dialout $USER
```


Intended for rpi4

output sync position over osc


si out de vnc
sudo sed -i '/CaptureTech/d' /root/.vnc/config.d/vncserver-x11
sudo vncserver-x11 -service -reload


jack and pulseaudio

```
sudo apt install pulseaudio-module-jack

```
Redirecting PulseAudio to JACK

On Debian-like systems, be sure to install pulseaudio-module-jack. (TODO: Add information about Fedora, OpenSuSE and the lot)

Then, use the following configuration if you intend to run jackd all the time:

~/.config/pulse/default.pa:

load-module module-native-protocol-unix
load-module module-jack-sink channels=2
load-module module-jack-source channels=2
load-module module-null-sink
load-module module-stream-restore
load-module module-rescue-streams
load-module module-always-sink
load-module module-suspend-on-idle
set-default-sink jack_out
set-default-source jack_in

The following isn't strictly necessary but might be useful. ~/.config/pulse/daemon.conf:

default-sample-format = float32le
default-sample-rate = 48000
realtime-scheduling = yes
exit-idle-time = -1

If you're using qjackctl as a launcher for jackd.

in settings options add a post startup script like this:

pactl load-module module-jack-sink channels=2; pactl load-module module-jack-source channels=2; pacmd set-default-sink jack_out
