
import argparse
import random
import time
import math
import threading
# import thread
from threading import Thread

from pythonosc import udp_client
from pythonosc.dispatcher import Dispatcher
from pythonosc import osc_server

# importing vlc module
import vlc
import os
import socket

vlc_instance = vlc.Instance('--input-repeat=99999999999 --quiet --fullscreen') 
media_player = vlc_instance.media_player_new()
starttime = time.time()
HOSTNAME=socket.gethostname()
MEDIA="0"




def get_player_state(unused_addr, args):
    print(f"media_filename:{MEDIA}")
    client.send_message(f"/{HOSTNAME}/media/filename", MEDIA)   # Send float message
    print(f"media_length:{media_player.get_length()}")
    client.send_message(f"/{HOSTNAME}/media/length", media_player.get_length())   # Send float message

def set_player_position(unused_addr, args):
    media_player.set_position(args)

def set_player_saturation(unused_addr, args):
    media_player.video_set_adjust_float(vlc.VideoAdjustOption.Saturation, args);

def set_player_audio_volume(unused_addr, args):
    media_player.audio_set_volume(int(args*100))
    #media_player.set_position(args)

def set_player_media_file(unused_addr, args):
    print(args)
    media_player.set_media(args)
    #media_player.set_position(args)

def print_fader_handler(unused_addr, args, value):
    print("[{0}] ~ {1:0.2f}".format(args[0], value))


def print_xy_fader_handler(unused_addr, args, value1, value2):
    print("[{0}] ~ {1:0.2f} ~ {2:0.2f}".format(args[0], value2, value1))

def start_server(ip, port):

    print("Starting Server")
    server = osc_server.ThreadingOSCUDPServer((ip, port), dispatcher)
    print("Serving on {}".format(server.server_address))
    thread = threading.Thread(target=server.serve_forever)
    thread.start()

    
def threaded_get_media_info():
    loop_notified=0
    time.sleep(0.5)
    get_player_state(0,0)


    while True:
        time.sleep(0.1 - ((time.time() - starttime) % 0.1))
        client.send_message(f"/{HOSTNAME}/media/time", media_player.get_time())   # Send float message
        client.send_message(f"/{HOSTNAME}/media/position", media_player.get_position())   # Send float message
        if media_player.get_time() < 600 and loop_notified == 0 :
            print("loop_notify")
            loop_notified=1
            client.send_message(f"/{HOSTNAME}/media/loopnotify", 1)   # Send float message
            client.send_message(f"/{HOSTNAME}/media/length", media_player.get_length())   # Send float message
        elif media_player.get_time() > 600 and loop_notified==1:
            loop_notified=0   
    



if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--serverip", default="127.0.0.1", help="The ip to listen on")
    parser.add_argument("--serverport", type=int, default=5005, help="The port the OSC Server is listening on")
    parser.add_argument("--clientip", default="127.0.0.1", help="The ip of the OSC server")
    parser.add_argument("--clientport", type=int, default=5006, help="The port the OSC Client is listening on")
    parser.add_argument("--media", default="~/Videos/test-ecran.mp4", help="The media to listen read")
    parser.add_argument("--startoffset",type=float, default=0, help="Start position offset at init (float)")
    args = parser.parse_args()


    # listen to addresses and print changes in values 
    dispatcher = Dispatcher()
    dispatcher.map("/get_player_state", get_player_state)
    dispatcher.map("/set_player_position", set_player_position)
    dispatcher.map("/set_player_audio_volume", set_player_audio_volume)
    dispatcher.map("/set_player_saturation", set_player_saturation)
    dispatcher.map("/set_player_media_file", set_player_media_file)
    dispatcher.map("/ping", print)

    # media object
    media = vlc_instance.media_new(args.media)
    # setting media to the media player
    media_player.set_media(media)

    # start playing video
    media_player.play()
    #media_player.video_set_adjust_int(vlc.VideoAdjustOption.Enable, 1)

    MEDIA=os.path.basename(args.media)
    media_player.toggle_fullscreen()
    media_player.set_position(args.startoffset);
    start_server(args.serverip, args.serverport)
    #start_client(args.clientip, args.clientport)
    client = udp_client.SimpleUDPClient(args.clientip, args.clientport, True)
    thread = Thread(target=threaded_get_media_info)
    thread.start()
