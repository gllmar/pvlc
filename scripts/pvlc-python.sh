#!/bin/bash
SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

#MEDIA=""$1"
#echo "media=$MEDIA"

# Check if the argument is provided
if [ -z "$2" ]; then
    # Default value if argument is not provided
    STARTOFFSET=0
else
    STARTOFFSET="$2"
fi


cd "$SCRIPTPATH"
cd ..

python pvlc.py  --clientip 255.255.255.255 --media $1 --startoffset $STARTOFFSET
