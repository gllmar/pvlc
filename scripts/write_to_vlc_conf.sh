#!/bin/bash


if [[ -z "$2"  ]]; then
    >&2 echo "No startoffset provided, default to 0"
    STARTOFFSET=0
    else 
     STARTOFFSET=$2
fi
echo "MEDIA=$1" > ~/.config/pvlc-python.conf
echo "STARTOFFSET=$STARTOFFSET" >> ~/.config/pvlc-python.conf

cat  ~/.config/pvlc-python.conf

systemctl --user restart pvlc-python.service 
