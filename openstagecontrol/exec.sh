#!/bin/bash
SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

LOAD="$SCRIPTPATH"/pvlc-osc.json 
SEND=127.0.0.1:10011
OSCPORT=10010
WEBPORT=8080

echo "	http://$HOSTNAME.local:$WEBPORT"

node "$SCRIPTPATH"/openstagecontrol_d/open-stage-control_node/index.js -s $SEND -l $LOAD -o $OSCPORT -p $WEBPORT --no-qrcode --no-gui


